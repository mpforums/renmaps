# UltraAOW.com TT File Repository Contribution Guidelines

## Map Removals

* Use the [issue tracker](https://gitlab.com/mpforums/renmaps/issues) to 
* Please be specific on how to reproduce the problem.
* Do not submit duplicate issues. Search first.
* Avoid including multiple requests/bugs into a single issue.

## Map Additions

Use a standard git forking workflow to submit maps to the repository root and submit a pull request to have your map added to the repository.

Please propose a tt.cfg game definition in the pull-request comments.